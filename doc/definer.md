DEFINER
=======

With the definer you can define the position, size and much more of each individual key of your keyboard.

File format description
-----------------------

A definer file is a simple json data structure consisting of three different parts explained below.

### meta
The meta part is used to give room for some generic meta data like date and time of creation, a name etc. This data may be enhanced in arbitrary ways. However, it is not further evaluated.

### defaults
Each individual key inherits the default data given in this defaults part. An empty key would equal the default key description.

* **x** the x, or horizontal position, starting at 0 on the left hand side
* **y** the y, or vertical position, starting at 0 from the top
* **width** the width of the key
* **height** the height of the key
* **weight** the weight of the key, a number representing the difficulty to press this particular key. A higher is more difficult.
* **kuinmm** all the above measures are given in a pseudo unit, the *key-unit*, this value is the measure of one key-unit in millimetre.  
* Currently unsupported **z**, **yaw**, **pitch**, **roll**, **shape**

### keyboard, groups, keys
The keyboard consists of keys put together as groups. Each group consists of probably a lot of keys, i.e. unique names to identify the key and any of the data given in the defaults part. Any data not given explicitly on the individual key is inherited from its parent group and then from the defaults parts.

This gives us the opportunity to put some hierarchy on to our keyboard definition. Think of Keyboard -> Numblock -> Zero-Key.

File format example
-------------------

```javascript
{
  "meta": {
    "author": "jolo",
    "date": "2016-06-03T10:29:56+02:00",
    "description": "My example keyboard definition",
  },
  "defaults": {
    "name": "Keyboard Example",
    "x": 0,
    "y": 0,
    "z": 0,
    "yaw": 0,
    "pitch": 0,
    "roll": 0,
    "weight": 1,
    "width": 1,
    "height": 1,
    "shape": "rectangle",
    "kuinmm": 12.5,
  },
  "keyboard": [
    {
      "name": "Block A",
      "keys": [
        {
          "name": "Key A1",
          "x": 0,
          "y": 0,
        },
        {
          "name": "Key A2",
          "x": 1,
          "y": 0,
        }
      ]
    },
    {
      "name": "Block B",
      "keys": [
        {
          "name": "Key B1"
          "x": 0,
          "y": 2,
          "width": 2,
        },
      ]
    }
  ]
}
```
