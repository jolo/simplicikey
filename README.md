SIMPLICIKEY
===========

Simplicikey (sk) is a set of tools to change the way we interact with computers via a keyboard type of input device. It provides means to define a keyboard geometry, a key layout and way to optimize this layout against a given set of *data*.

Contents
--------
* **sk-definer** Define your keyboard's geometry and the weights of each key, which is a number to represent how hard it is for you to press this particular key.
* **sk-layouter** Define your keyboard's key layout, or any restrictions that should be applied to individual key combinations.
* **sk-lexis** Provide statistics of common vocabulary and further provide means to extract this type of statistics from any kind of source.
* **sk-optimizer** Uses a keyboard's geometry definition and layout definition against the statistical data from lexis to analyze and/or optimize the *key layout*.
* **sk-designer** The GUI for Simplicikey. 

### Planned
* **sk-geometry-optimizer** Uses a keyboard's geometry definition and layout definition against the statistical data from lexis to analyze and/or optimize the *keyboard geometry*.

Use Case
--------
Think about the hype that is currently disrupting the geek scene with all these  nerdy keyboards being invented and made available on some crowd-funding platforms or being made in some makers shed [1].

Unfortunately most of the people still rely on some staggered QERTY-style keyboard enhanced by some programmer's or function key layer. Sadly only few use something to really reduce the strain that is induced by reaching to often used keys that are far away and so on. However, there is a considerable number of alternative keyboard layouts [2] and obviously more ergonomic key layouts [3].

With all the power given by 3D printing, rapid prototyping, cheap electronic kits already used for custom keyboards [4] why not further optimize the way we type?

1. Atom, Plank, ErgoDox Family, etc.
2. Colemak, Dvorak, Neo2, ADNW, etc.
3. Maltron, Kinesis, Truly Ergonomic, etc.
4. [Teensy](https://www.pjrc.com/teensy/)

A word on ingenuity
-------------------
Do not mistake this project for a genuine idea. Searching for optimized keyboard layouts has begun as early as the invention of modern electronic typewriters. Optimizing the key placement has lead to truly ingenious keyboards like the Maltron and Kinesis.

* http://www.daskeyboard.com/blog/typing-through-time-the-history-of-the-keyboard/
* https://en.wikipedia.org/wiki/QWERTY
* https://en.wikipedia.org/wiki/Keyboard_layout

The basic parts of sk-definer/layouter/lexis/optimizer are already developed in the [Optimierer](http://www.adnw.de/index.php?n=Main.Downloads) software used for the [ADNW](http://www.adnw.de/) layout. On one hand the scientific approach of the weighing of key moves, key placement etc. is thorough and appreciated a lot. On the other hand the antique software realization prevented any sustainable attempts on improving or enhancing this application. However, wherever possible and applicable ideas, algorithms and even code is taken shamelessly from this *optimizer*.

> nanos gigantum humeris insidentes
