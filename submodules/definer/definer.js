function Keyboard() {
  this.name = "";
  this.author = "";
  this.date = "";
  this.description = "";
}

function Group() {
  this.name = "";
  this.x = 0;
  this.y = 0;
  this.angle = 0;
  this.weight = 0;
}

function Key() {
  this.name = "";
  this.x = 0;
  this.y = 0;
  this.angle = 0;
  this.weight = 1;
  this.width = 1;
  this.height = 1;
  this.shape = "rect";
  this.roundness = 5;
  this.kuinmm = 35;
}

var ergodox = new Keyboard();
ergodox.author = "jolo";
ergodox.date = "2016-06-09T17:31:14+02:00";
ergodox.name = "ergodox";
ergodox.description = "ErgoDox Infinity";

ergodox.left = new Group();
ergodox.left.name = "left";

ergodox.left.finger = new Group();
ergodox.left.finger.name = "finger";
for (column = 0; column < 7; column++) {
  for (row = 0; row < 5; row++) {
    var k = new Key();
    k.name = "c"+column+"r"+row;
    k.x = column;
    k.y = row;
    ergodox.left.finger.keys.push(k);
  }
}

ergodox.left.thumb = new Group();
ergodox.left.thumb.name = "thumb";
ergodox.left.thumb.x = 7;
ergodox.left.thumb.y = 6;
ergodox.left.thumb.angle = 15;
var k = new Key();
k.name = "t1";
k.x = 0;
k.y = 1;
ergodox.left.thumb.keys.push(k);
var k = new Key();
k.name = "t2";
k.x = 1;
k.y = 1;
ergodox.left.thumb.keys.push(k);
var k = new Key();
k.name = "t3";
k.x = 1;
k.y = 0;
ergodox.left.thumb.keys.push(k);
var k = new Key();
k.name = "t4";
k.x = 2;
k.y = 0;
ergodox.left.thumb.keys.push(k);
var k = new Key();
k.name = "t5";
k.x = 2;
k.y = 1;
ergodox.left.thumb.keys.push(k);
var k = new Key();
k.name = "t6";
k.x = 2;
k.y = 2;
ergodox.left.thumb.keys.push(k);
