//
// generic functions
//

"use strict;"

//
// merge individual parts of a keyboard definition in a sensible way:
// - for coordinates, weights and strings a and b are added
// - for width and heights a takes precedence over b
//
function keyboardMerge(a, b) {
  var c = JSON.parse(JSON.stringify(b)) // fast way to clone the object
  for (var element in a) {
    switch (element) {
      case "x":
      case "y":
      case "weight":
      case "name":
        c[element] += a[element]
        break;
      default:
        c[element] = a[element]
        break;
    }
  }
  return c
}

//
// create proper array of keys with all the definitions merged from keyboard
// defaults and groups from a definer keyboard object.
//
function flattenKeys(keyboard) {
  var key = JSON.parse(JSON.stringify(keyboard))

  if keyboard.hasOwnPropery(keys) {
    delete key.keys
  }

  if keyboard.hasOwnPropery(group) {
    delete key.group
  }

}
