// @TODO
// I would love to use the proper qwertz.def.json example file, but unfortunately
// it is impossible to parse a separate json file which is not provided by a true
// server, thus I use this cheap copy&paste/javascript-variable-file trick.
var qwertz = {
  "meta": {
    "date": "2016-06-03T10:29:56+02:00",
    "name": "QERTZ, DELL Keyboard KB212-B",
  },
  "defaults": {
    "x": 0,
    "y": 0,
    "z": 0,
    "yaw": 0,
    "pitch": 0,
    "roll": 0,
    "weight": 1,
    "width": 1,
    "height": 1,
    "shape": "rectangle",
    "kuinmm": 35,
  },
  "keys": [
    {
      "name": "esc",
      "x": 0,
      "y": 0,
    },
    {
      "name": "f1",
      "x": 1.75,
      "y": 0,
    },
    {
      "name": "f2",
      "x": 2.75,
      "y": 0,
    },
    {
      "name": "f3",
      "x": 3.75,
      "y": 0,
    },
    {
      "name": "f4",
      "x": 4.75,
      "y": 0,
    },
    {
      "name": "f5",
      "x": 6.5,
      "y": 0,
    },
    {
      "name": "f6",
      "x": 7.5,
      "y": 0,
    },
    {
      "name": "f7",
      "x": 8.5,
      "y": 0,
    },
    {
      "name": "f8",
      "x": 9.5,
      "y": 0,
    },
    {
      "name": "f9",
      "x": 11.25,
      "y": 0,
    },
    {
      "name": "f10",
      "x": 12.25,
      "y": 0,
    },
    {
      "name": "f11",
      "x": 13.25,
      "y": 0,
    },
    {
      "name": "f12",
      "x": 14.25,
      "y": 0,
    },
    {
      "name": "druck",
      "x": 15.75,
      "y": 0,
    },
    {
      "name": "rollen",
      "x": 16.75,
      "y": 0,
    },
    {
      "name": "pause",
      "x": 17.75,
      "y": 0,
    },
    {
      "name": "^",
      "x": 0,
      "y": 1.5,
    },
    {
      "name": "1",
      "x": 1,
      "y": 1.5,
    },
    {
      "name": "2",
      "x": 2,
      "y": 1.5,
    },
    {
      "name": "3",
      "x": 3,
      "y": 1.5,
    },
    {
      "name": "4",
      "x": 4,
      "y": 1.5,
    },
    {
      "name": "5",
      "x": 5,
      "y": 1.5,
    },
    {
      "name": "6",
      "x": 6,
      "y": 1.5,
    },
    {
      "name": "7",
      "x": 7,
      "y": 1.5,
    },
    {
      "name": "8",
      "x": 8,
      "y": 1.5,
    },
    {
      "name": "9",
      "x": 9,
      "y": 1.5,
    },
    {
      "name": "0",
      "x": 10,
      "y": 1.5,
    },
    {
      "name": "ß",
      "x": 11,
      "y": 1.5,
    },
    {
      "name": "´",
      "x": 12,
      "y": 1.5,
    },
    {
      "name": "lös",
      "x": 13.5,
      "y": 1.5,
      "width": 2,
    },
  ]
}
