//
// designer file
//

"use strict";

console.log("designer.js loaded")

var keys = flattenKeys(ergodox)

var svgc = d3.select("#sk-designer-main")
            .append("svg")
            .attr("width", 900)
            .attr("height", 600)
            .style("border", "1px solid silver")

var boxes = svgc.selectAll("rect")
            .data(keys)
            .enter()
            .append("rect")

var boxAttr = boxes
            .attr("x", function (d) { return 10+d.kuinmm*d.x; })
            .attr("y", function (d) { return 10+d.kuinmm*d.y; })
            .attr("rx", 300/defaults.kuinmm)
            .attr("ry", 300/defaults.kuinmm)
            .attr("width", function (d) { return 0.9*d.kuinmm*d.width; })
            .attr("height", function (d) { return 0.9*d.kuinmm*d.height; })
            .style("fill", "green")
            .style("stroke", "black")
            .style("stroke-width", 1)

boxes.on("mouseover", function() {
              d3.select(this)
                .transition()
                  .duration(150)
                  .style("stroke-width", 3)
            }).on("mouseout", function() {
              d3.select(this)
                .transition()
                  .duration(500)
                  .style("stroke-width", 1)
            })

var labels = svgc.selectAll("text")
            .data(keys)
            .enter()
            .append("text")
var textAttr = labels
            .attr("x", function (d) { return 15+d.kuinmm*d.x; })
            .attr("y", function (d) { return 12+d.kuinmm*d.height*0.5+d.kuinmm*d.y; })
            .text(function (d) { return d.name; })
